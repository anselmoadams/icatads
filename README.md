# ICATADS

Biblioteca de funções consolidadas para MatLAB/Gnu Octave, utilizadas nas aulas
da disciplina TI148 - Inteligência Computacional Aplicada, criadas e/ou compiladas pelo 
Prof. Dr. Roberto Tadeu Raittz. 

As funções consolidadas encontram-se no diretório MatLIB, e deve ser incluso no caminho de pesquisa
do MatLAB/Gnu Octave para uso. 

## Download

Escolha um formato de arquivo para download:

* [Arquivo .zip](https://gitlab.com/anselmoadams/icatads/repository/archive.zip?ref=master)
* [Arquivo .tar](https://gitlab.com/anselmoadams/icatads/repository/archive.tar?ref=master)
* [Arquivo .tar.gz](https://gitlab.com/anselmoadams/icatads/repository/archive.tar.gz?ref=master)
* [Arquivo .tar.bz2](https://gitlab.com/anselmoadams/icatads/repository/archive.tar.bz2?ref=master)

Ou clone o repositório: 

```bash
[user@localhost] git clone https://gitlab.com/anselmoadams/icatads.git
```

## Instruções - Linux/Gnu Octave

Copie o arquivo octaverc-linux-example para $HOME/.octaverc.
Verifique os caminhos nas funções cd e addpath(), alterando-os se necessário.
Ao iniciar o Gnu Octave, as funções da biblioteca estarão disponíveis para uso.


## Instruções - Windows 

Contribua com instruções para windows :) 
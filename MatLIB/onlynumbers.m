function mret = onlynumbers(xstr)
s = xstr;
s = s(s>47 & s<58);
if ~isempty(s)
    mret = srt2num(s);
else
    mret = 0;
end    

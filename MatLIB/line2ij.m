function mret = line2ij(nlin, m)
% gera pares de indices i, j para cada posi��o dos numeros 1:nlin
i = fix(((1:nlin)-1)/m)+1;
j = mod(((1:nlin)-1),m)+1;
mret = [i' j'];
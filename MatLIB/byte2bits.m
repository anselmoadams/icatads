function mret = byte2bits(v)
v = double(v);
n = length(v);
mret = zeros(n,8);
vaux = v;
for i=1:8
    i1 = ~(fix(vaux/2)==(vaux/2));
    vaux = fix(vaux/2);
    mret(i1,i) = 1;
end    
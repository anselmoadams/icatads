function mret = str2bits(str)
%converte a matriz de strings str em uma matriz correspondente de bits por
%linhas
v = double(str-0);
n = length(v(:,1));
m = length(v(1,:));
mret = zeros(n,8*m);
vaux = v;
for i=1:8
    i1 = ~(fix(vaux/2)==(vaux/2));
    vaux = fix(vaux/2);
    mret(:,i:8:m*8) = i1;
end    
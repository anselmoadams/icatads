function mret = w2pca(w,varargin)
n = length(w(1,:))-1;
if ~isempty(varargin)
    n = varargin{1};
end    
c = corr(w(:,1:end-1));
[a b] = eig(c);
[xx ii] = sort(diag(b),'descend');
A = a(:,ii(1:n));
mret = A; %Autovetores dos componentes principais ordenados por relevancia

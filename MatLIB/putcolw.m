function mret = putcolw(w)
% Adiciona coluna com 1 em w
mret = [w w(:,end)*0+1];
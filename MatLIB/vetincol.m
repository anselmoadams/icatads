function mret = vetincol(vet)
%retorna o vetor sempre em coluna
s = size(vet);
mret = vet;
if (s(2)>s(1))
    mret = vet';
end    
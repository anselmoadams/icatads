function mret = numbsinstr(xstr)
xstr(xstr<48 | xstr>57) = 32;
mret = str2num(xstr);
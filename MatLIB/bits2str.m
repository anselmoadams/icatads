function mret = bits2str(u)
%converte a matriz de bits em uma matriz correspondente de bits por
%linhas
mret = char(vet2mat(bits2bytes(mat2vet(u)),round(length(u(1,:))/8)));
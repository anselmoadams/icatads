function mret = mmvfuzzy2(vect, vz, varargin)
%%Media movel fuzzy melhorada usando fun��o triangular para os dois lados
%% Generalizada em 02/09/2014 para funcionar com matrizes de vetores
%%
%% Roberto 30-10-2013
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ntimes = 1;
if ~isempty(varargin)
    ntimes = varargin{1};
end
for itm=1:ntimes
    n = length(vect(1,:));
    m = length(vect(:,1));
    s = 2*vz+1;
    t = triang(s)';
    svect = zeros(m,n);
    for i=1:n
        d = (i-vz-1);
        t0 = 1;
        if d<=0
            k = vz + d;
            t0 = vz - k + 1;
        end
        d = (n-(i+vz));
        tE = s;
        if d<=0
            tE = s + d;
        end
        i0 = max(i-vz,1);
        iE = min(i+vz,n);
        %
        a = vect(:,i0:iE);
        b = repmat(t(t0:tE),m,1);
        c = sum(t(t0:tE));
        %
        svect(:,i) = sum(a.*b,2)/c;
    end    
    mret = svect;
    vect = mret;
end    
    
function mret = strcelllines(M)
% concatena colunas em uma cell de strings j>1
xindsC = mat2celllines(vet2mat(find(ones( length(M(:,1)) , length(M(1,:))  )), length(M(:,1)))');
mret = cellfun(@(x) bs2b(char(mat2vet(char(M(x))))),xindsC,'UniformOutput',false);
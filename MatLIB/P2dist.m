function [M d] = P2dist(P)
% M � a matriz das distancias rec�procas entre os pontos de P
n = length(P(:,1));
cb = combinav1v2(1:n,1:n);
d = normavect(P(cb(:,1),:)-P(cb(:,2),:));
M = zeros(n,n);
M(1:(n*n)) = d;
function mret = kmeans_tr(w,cvect)
% centros para kmeans de w (sem classe) para cada numero de centros do vetor cvect
% para utilizar em kmeans_tr(wx,rede)
% ex.: kmeans_tr(w0,[2 3 5]);
n = length(w(:,1));
ncv = length(cvect);
rede.cvect = cvect;
wk = zeros(n, ncv);
for i=1:ncv
    [wi ci] = kmeans(w, cvect(i));
    rede.centros(i).ci = ci;
    wk(:,i) = wi;
end
rede.wk = wk;
mret = rede;
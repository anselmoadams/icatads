function [mret inorm] = versor(vets)
% Transforma vets (linhas) em seus versores
m = length(vets(1,:));
inorm = sqrt(sum(vets.^2,2));
mret = vets./repmat(inorm,1,m);
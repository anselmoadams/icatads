function mret = bits2bytes(xbits)
%...
mbits = vet2mat(xbits,8);
mret = (sum((mbits .* repmat(2.^(0:7),length(mbits(:,1)),1))'));
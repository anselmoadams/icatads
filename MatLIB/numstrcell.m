function mret = numstrcell(vect)
% Cria cell com strings correspondentes aos numeros em vect
mret = mat2celllines(num2str(vetincol(vect)));
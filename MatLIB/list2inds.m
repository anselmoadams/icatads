function [vetuniq xcell] = list2inds(xlist)
% Retorna indices xlist(:,1):xlist(:,2); vetuniq contem o vetor com todos
% os indices encontrados; xcell as listas de indices para cada intervalo de
% xlist
%
xcell = cellfun(@(x) (min(x):max(x)),mat2celllines(xlist), 'UniformOutput',false)';
vetuniq = unique(cell2mat(xcell));
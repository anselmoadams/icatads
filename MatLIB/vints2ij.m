function mret = vints2ij(vints, m)
% gera pares de indices i, j para cada posi��o dos numeros 1:nlin
%%%
i = fix(vetincol(vints-1)/m)+1;
j = mod(vetincol(vints-1),m)+1;
mret = [i j];
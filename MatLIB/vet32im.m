function mret = vet32im(image, vet)
sz = size(image);
qd = prod(sz(1:2));
v3 = find(image==image);
vet = mat2vet(vet');
image(v3) = vet;
mret = image;
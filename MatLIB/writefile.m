function mret = writefile(matstring, xfile)
n = length(matstring(:,1));
fid = fopen(xfile,'wt');
for i=1:n
    fprintf(fid,'%c',matstring(i,:));
    fprintf(fid,'\n');
end
fclose(fid);

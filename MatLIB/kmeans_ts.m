function mret = kmeans_ts(w, rede)
% retorna colunas de w baseadas nos centros gerados de acordo com rede (de
% kmeans_tr(w0,vcentros)
%
sz = size(w);
n = sz(1);
ncv = length(rede.cvect);
wcols = zeros(n,ncv);
for i=1:ncv
    c = rede.centros(i).ci;
    wcols(:,i) = getkmeansclass(w,c);
end
mret = wcols;
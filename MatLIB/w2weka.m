function mret = w2weka(w,varargin)
%w2weka converte uma matriz que cont�m dados de treinamento
%
% As colunas de 1:n-1 s�o representam os atributos deste conjunto;
% A �ltima coluna (n) representa o atributo classificador do registro sendo
% que as colunas de 1:n-1 representam atributos normais.
% Cada linha representa um registro deste conjunto de treinamento
%
%
% varargin{1} deve ser o nme do arquivo para salvar o arquivo no formato
% WEKA.

n = length(w(:,1));
m = length(w(1,:));
xcab = char(' '*ones(3+m,80));
sw = [];

%
xcom = 'Dados Gerados pela fun�ao w2weka';
xtit = 'VariavelMatlab';
xdat = 'data';
%
xcab(1,1:length(xcom)+1) = ['%' xcom];
xcab(2,1:length(xtit)+10) = ['@Relation ' xtit];
xcab(m+3,1:length(xdat)+1) = ['@' xdat];
%
for i=1:m-1
    vcol = char(','*(ones(n,1)));
    sw = [sw num2str(w(:,i)) vcol];
    %
    xs = ['@attribute ATR' num2str(i) ' real'];
    xcab(2+i,1:length(xs)) = xs;
end
%
scl = '{';
cls = class_in_w(w);
for i=1:length(cls)
    scl = [scl num2str(cls(i)) ','];
end
scl(length(scl)) = '}';
%
xs = ['@attribute class ' scl];
xcab(2+m,1:length(xs)) = xs;
sw = [sw num2str(w(:,m))];
%
lcab = length(xcab(:,1));
lsw = length(sw(:,1));
ccab = length(xcab(1,:));
csw = length(sw(1,:));
%
nf = lcab+lsw;
mf = max(ccab,csw);
mout = char(' '*ones(nf,mf));
mout(1:lcab,1:ccab) = xcab;
mout(1+lcab:end,1:csw) = sw;
%
if ~isempty(varargin)
    writefile(mout,[varargin{1} '.arff']);
end
mret = mout;
end
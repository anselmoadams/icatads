function mret = bitrand(n,m)
mret = cast(cast(rand(n,m),'uint16'),'double');
function mret = onlyletters(xstr, varargin)
% Substitui caracteres em xstr que n�o s�o letras por espa�o
putnumoff = [];
if ~isempty(varargin)
    if varargin{1}==1 %Coloca numeros
        putnumoff = 48:57;
    end
end
%Com alguns sinais notltr = [247 215 216 123:191 91:96 1:35 37:44 46:47 58:64 putnumoff];
notltr = [247 215 216 123:191 91:96 1:47 58:64 putnumoff];
inl = ismember(xstr,notltr);
mret = xstr;
mret(inl) = ' ';
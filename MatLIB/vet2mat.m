function mret=vet2mat(vect,larg)
%cria matriz com os valores de vet por linha com larg colunas (completa ultima linha com zeros)
nv = length(vect);
n = ceil(nv/larg);
mat = zeros(larg,n);
imat = find(mat==mat);
mat(imat(1:nv)) = vect;
mret = mat';

function mret = wsort(w)
% scramble w (vector ou cell) randomly 
%  
[xx, id] = sort(rand(length(w(:,1)),1));
mret = w(id,:);
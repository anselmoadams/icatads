function m_ret = slquery( query, FAN_RNA_FILE, varargin )

% query - conjunto de vetor de caracteristicas a ser classificado
% FAN_RNA_FILE - arquivo da rede treinada no EasyFAN
% opcionais
%  parametros para o classificador... uso futuro
%
warning off

mret = struct();

if nargin > 1

	if (isempty(query))
		m_ret.nome = 'Query is empty';
		return
	end

	if (exist( FAN_RNA_FILE, 'file') == 0)
		m_ret.nome = 'FAN File not found';
		return;
	end
else 
	m_ret.nome = 'Not enough arguments';
	return;
end

tmpNom = [ 'slq' num2str(ceil(rand(1,1)*100)) '.txt' ];
arq2 = [ 'res' num2str(ceil(rand(1,1)*100)) '.txt' ];

m_txt = vet2str( query );

fid = fopen( tmpNom , 'w+t');
n = length( m_txt(:,1) );
for i=1:n
    fprintf(fid, '%s\n', m_txt(i,:) );
end
fclose(fid);

m_ret.Query = query;
m_ret.FAN = FAN_RNA_FILE;

umfn = mfilename('fullpath');

if ispc()
	%Windows Operating System
	umfn(find(umfn=='\')) = '/';
end

umfndir = umfn(1:strfind(umfn,'/slquery')(1:length(strfind(umfn,'/slquery'))));


if(exist([pwd '/SLQuery.jar']))
    cmd = ['java -jar SLQuery.jar -i ' tmpNom ' -f ' FAN_RNA_FILE ' -o ' arq2];
else
    wherenet = [umfndir '/' FAN_RNA_FILE];
    if ~exist(wherenet,'file')
        wherenet = [pwd '/' FAN_RNA_FILE];
    end
    cmd = ['java -jar ' umfndir '/SLQuery.jar -i ' tmpNom ' -f ' wherenet ' -o ' arq2];
end
[blastStatus, blastResult] = system(cmd);

if( exist( arq2 , 'file' ) ~= 0 )
    fid = fopen(arq2, 'rt' );
    fseek(fid, 0, 'eof');
    qde = ftell(fid);
    fseek(fid, 0 , 'bof');
    result = fread(fid, qde, '*char' )';
    fclose(fid);
    s = regexp( result , [ '<nome>(?<nome>[^<]*)</nome><valor>(?<valor>[^<]*)</valor>'  ], 'names' );
    if ~isempty(s)
        m_ret.S = s;
        k = (([m_ret.S.valor]));
        if ~iscell(k)
            k(k==',') = '.';
            k = str2num(k);
            k = k';
        else
            k = (cell2mat(k'));
            k(k==',')='.';
            str2num(k);
        end
        m_ret.Result = k;
    else
        m_ret.nome = 'vazio';
        m_ret.valor = -1;
    end
    delete( arq2 );
end
end

function mret = vet2str(t)
n = length(t(:,1));
m = length(t(1,:));
virgulas = char(ones(1,n)*44)';
mret = [];
for j=1:m-1
    s = num2str(t(:,j));
    mret = [mret s virgulas];
end
s = num2str(t(:,j+1));
mret = [mret s];

end
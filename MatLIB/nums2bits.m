function mret = nums2bits(v, varargin)
%transforma os numeros inteiros em v em bits reservando varargin{1} bits
%para o codigo (ncod) do numero de bytes que s�o necessarios para a representa��o
%do maior n�mero do vetor; usar em conjunto com bits2bytes.
if isempty(varargin)
    ncod = 0;
else
    ncod = varargin{1};
end    
n = length(v);
x = max(v);
nbits = round(0.5+log(x)/log(2));
nbytes = round(0.49999999+(nbits+ncod)/8);

mret = zeros(n,nbytes*8);
vaux = v;
for i=1:nbytes*8
    i1 = ~(fix(vaux/2)==(vaux/2));
    vaux = fix(vaux/2);
    mret(i1,i) = 1;
end    

lig = byte2bits(nbytes);
mret(nbytes*8-ncod+1:nbytes*8) = lig(1:ncod);
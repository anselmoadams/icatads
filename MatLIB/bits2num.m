function mret=bits2num(bits)
%converte para um numero decimal para cada linha de bits um numero
bits = logical(bits);
m = length(bits(1,:));
n = length(bits(:,1));
mret = sum(repmat((2.^((1:m)-1)),n,1).*bits,2);

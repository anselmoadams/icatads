function mret = normavect(mat1)
%Calcula a norma 2 por lina de mat
 mret = sqrt(sum(mat1.^2,2));
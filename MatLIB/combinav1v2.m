function mret = combinav1v2(v1, v2)
% Combina elementos de v1 com cada elemento de v2
sz = size(v1);
if sz(2)>sz(1)
    v1 = v1';
end    
sz = size(v2);
if sz(2)>sz(1)
    v2 = v2';
end    
n1 = length(v1);
n2 = length(v2);
mret = [sort(repmat(v1,n2,1)) repmat(v2,n1,1)];
function mret1 = polifan_tr(wo,epcs,range, d, varargin)
%Treina rede FAN para o conjunto (mat or cell) de padroes em wo epcs vezes
%A classe deve ser um inteiro maior que zero
%Retorna vetor auto testes para cada epoca (result) com respectivo
%treinamento
%Os valores das caracteristicas nas cell devem estar em vetores "linha"
if iscell(wo)
    w = wo;
else    
    w = num2cell(wo);
end    
%
m = length(w(1,:));
n = length(w(:,1));
epoc = [];
rs = [];
%Se n�o recebe vfan inicializa estrutura
%...
%Em teste 1
corte = round(0.70*n); %Parametrizar Parametrizar
w_tr = w; %w(1:corte,:);
w_ts = w; %w(1+corte:end,:);
%w_tr = w;
%w_ts = w;
w = wsort(w_tr);
%
n = length(w(:,1));
%Fim teste  1
if isempty(varargin)
%    range = 2000; %Arrumar Generalizar arrumar
%    d = 6;
    %
    nfeat = m;
    classes = cell2mat(w(:,m));
    nclas = length(unique(classes));
    %
    fans = abs(rand(nclas,nfeat,range));
    fsum = zeros(nclas,nfeat);
    %
    %Normaliza
    zmax = zeros(1,m);
    zmin = zeros(1,m);
    for j=1:nfeat-1
        zmax(j) = max([w{:,j}]);
        zmin(j) = min([w{:,j}]);
        w(:,j) = cellfun(@(x) range*(x - zmin(j))/(zmax(j)-zmin(j)), w(:,j), 'UniformOutput',false);
    end    
    %
    for k=1:nclas
        for j=1:nfeat
            fsum(k,j) = sum(fans(k,j,:));
        end
    end    
    %
    mret.range = range;
    mret.d = d;
    mret.nfeat = nfeat;
    mret.nclas = nclas;
    mret.fans = fans;
    mret.fsum = fsum;
    mret.eps = 0.002;
    mret.zmax = zmax;
    mret.zmin = zmin;
    vet = [];
    %
else
    mret = varargin{1};
    range = mret.range;
    d = mret.d;
    nfeat = mret.nfeat;
    nclas = mret.nclas;
    fans = mret.fans;
    fsum = mret.fsum;
    eps = mret.eps;
    zmax = mret.zmax;
    zmin = mret.zmin;
    vet = mret.vet;
    for j=1:nfeat-1
        w(:,j) = cellfun(@(x) range*(x - zmin(j))/(zmax(j)-zmin(j)), w(:,j), 'UniformOutput',false);
    end    
end    
for l=1:epcs
    w = wsort(w);
    l
    for i=1:n
        for j=1:(m-1)
            mret.fans = fans;
            mret.fsum = fsum;
            u = w{i,j};
            jfuzzy = triangf(u,d,range);
            jsum = sum(jfuzzy);
            fc = ftests(w(i,:),mret);
            cl = w{i,m};
            fu = fans(cl,j,:);
            fu = fu(1,:);
            delta = (jfuzzy/jsum).^0.2;
            fu = fu + delta;
            fans(cl,j,:) = fu;
            fsum(cl,j) = fsum(cl,j) + sum(delta);
            fcl = fc.out;
            vet = [vet; fc.vet']; %Concatena resultados em cada �poca
            if fcl~=cl
                fu = fans(fcl,j,:);
                sfu = fsum(fcl,j);
                sfu = sfu(1,1);
                fu = fu(1,:);
                fu = (fu.*(1-mret.eps*jfuzzy/jsum)); %Original
%                fu = fu.*(1-mret.eps*(jfuzzy/(sfu*jsum)));   %.^0.5;%Parece melhor
                fans(fcl,j,:) = fu;
            end
        end
    end    
    mret.fans = fans;
    mret.fsum = fsum;
    mret.vet = vet;
%    result = polifan_ts(wo,mret); %voltar linha abaixo
    result = polifan_ts(w_ts,mret);
    epoc(l).data = mret;
    epoc(l).result = result;
    rs = [rs result.certos];
    subplotfan(mret);
end
[xx ii] = sort(rs,'descend');
Z.Net = epoc(ii(1)).data;
Z.Best = epoc(ii(1)).result;
Z.EPCs = epoc;
Z.Evol = rs;
mret1 = Z;
    


function mret = w2cell(w)
n = length(w(:,1));
m = length(w(1,:));
mret = mat2cell(w,ones(n,1),ones(1,m)); %Use num2cell
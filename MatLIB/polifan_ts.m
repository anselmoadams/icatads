function mret = polifan_ts(wo,vfan)
if iscell(wo)
    w = wo;
else    
    w = num2cell(wo);
end    
m = length(w(1,:));
n = length(w(:,1));
nfeat = m;
%Normaliza
zmax = vfan.zmax;
zmin = vfan.zmin;
for j=1:nfeat-1
    zmax(j) = max([w{:,j}]);
    zmin(j) = min([w{:,j}]);
    w(:,j) = cellfun(@(x) vfan.range*(x - zmin(j))/(zmax(j)-zmin(j)), w(:,j), 'UniformOutput',false);
end    
resp = zeros(n,3);
for i=1:n
    ret = ftests(w(i,:),vfan);
    resp(i,1) = ret.out;
    resp(i,2) = w{i,m};
    resp(i,3) = ret.xx;
end
%mret = resp;
%
%Gera aida com matconf    
%
out = resp(:,1);
T = resp(:,2);
%
nc = vfan.nclas;
if min(T) == 1
    T = T-1;
    out = out - 1;
    nc = nc-1;
end
out(find(out>nc)) = nc;
out(find(out<0)) = 0;
conf = zeros(nc+1,nc+1);
for i=1:n
    conf(T(i)+1,out(i)+1) = conf(T(i)+1,out(i)+1)+1;
end  
mret.conf = conf;
r = [];
for i=1:(nc+1)
    u = conf(i,i)/sum(conf(:,i));
    r = [r u];
end    
certos = 100*(sum(out==T))/n;
mret.certos = certos;
mret.prop = r;
mrert.resp = resp(:,1:2);


    